package com.example.Prueba1.repository;

import com.example.Prueba1.model.Vehicle;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Transactional
@Repository
public interface VehicleRepository extends JpaRepository<Vehicle, Long> {

}

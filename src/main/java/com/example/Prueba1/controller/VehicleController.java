package com.example.Prueba1.controller;

import com.example.Prueba1.model.Vehicle;
import com.example.Prueba1.service.VehicleService;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.swing.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;

@RestController
@RequestMapping("/api/db")
public class VehicleController {

    private final Logger log = LoggerFactory.getLogger(VehicleController.class);

    private final VehicleService vehicleService;

    public VehicleController(VehicleService vehicleService) {
        this.vehicleService = vehicleService;
    }

    @GetMapping("/vehicles")
    Collection<Vehicle> listAllVehicles() {
        return vehicleService.list();
    }

    /**
     * {@code GET  /vehicles/:id} : get the "id" vehicle.
     * @param id the id of the vehicle to retrieve
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the vehicle, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/vehicles/{id}")
    Vehicle getVehicleById(@PathVariable long id) {
        return vehicleService.getById(id).map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build()).getBody();
    }

    /**
     * {@code POST  /vehicles} : Create a new vehicle.
     * @param vehicle the vehicle to create
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new vehicle, or with status {@code 400 (Bad Request)} if the vehicle has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/vehicle")
    ResponseEntity<Vehicle> createVehicle(@Valid @RequestBody Vehicle vehicle) throws URISyntaxException {
        log.info("Request to create vehicle: {}", vehicle);
        Vehicle result = vehicleService.save(vehicle);
        return ResponseEntity.created(new URI("/api/db/vehicles/" + result.getId()))
                .body(result);
    }

    /**
     * {@code PUT  /vehicles} : Updates an existing vehicle.
     * @param vehicle the vehicle to update
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated vehicle,
     */
    @PutMapping("/vehicle/{id}")
    ResponseEntity<Vehicle> updateVehicle(@Valid @RequestBody Vehicle vehicle) {
        log.info("Request to update vehicle: {}", vehicle);
        Vehicle result = vehicleService.save(vehicle);
        return ResponseEntity.ok().body(result);
    }

    /**
     * {@code DELETE  /vehicles/:id} : delete the "id" vehicle.
     * @param id the id of the vehicle to delete
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/vehicle/{id}")
    public ResponseEntity<?> deleteVehicle(@PathVariable Long id) {
        log.info("Request to delete vehicle: {}", id);
        vehicleService.delete(id);
        return ResponseEntity.ok().build();
    }

    /**
     * {@code GET  /generate/:n} : generate n vehicles.
     * @param n number of vehicles to generate
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @GetMapping("/generate/{n}")
    public ResponseEntity<?> generateVehicles(@PathVariable int n) {
        log.info("Request to generate {} vehicles", n);
        vehicleService.generateVehicles(n);
        return ResponseEntity.ok().build();
    }
    @PostMapping("/vehicle/Consultation")
    public void consultationVehicle(@PathVariable Spring Consultation, @PathVariable Long id ) {
        Vehicle vehicle = vehicleService.getById(id).get();
        vehicle.setPopularity(vehicle.getPopularity()+1);
        vehicleService.save(vehicle);
    }

    /**
     * {@code GET  /vehicles/:price} : get vehicles with price less than or equal to the given price.
     * @param price price to filter
     * @return vehicles with price less than or equal to the given price
     */
    @GetMapping("/vehicles/{price}")
    public ResponseEntity<?> getVehiclesByPrice(@PathVariable int price) {
        log.info("Request to get vehicles by price: {}", price);
        return ResponseEntity.ok().body(vehicleService.filterByPrice(price));
    }

    /**
     * {@code GET  /vehicles/:type} : get vehicles with type equal to the given type.
     * @param type type to filter
     * @return vehicles with type equal to the given type
     */
    @GetMapping("/vehicles/{type}")
    public ResponseEntity<?> getVehiclesByType(@PathVariable String type) {
        log.info("Request to get vehicles by type: {}", type);
        return ResponseEntity.ok().body(vehicleService.filterByType(type));
    }

    /**
     * {@code GET  /vehicles/:color} : get vehicles with brand equal to the given brand.
     * @param color color to filter
     * @return vehicles with color equal to the given color
     */
    @GetMapping("/vehicles/{color}")
    public ResponseEntity<?> getVehiclesByColor(@PathVariable String color) {
        log.info("Request to get vehicles by color: {}", color);
        return ResponseEntity.ok().body(vehicleService.filterByColor(color));
    }
}

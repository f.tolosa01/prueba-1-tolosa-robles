package com.example.Prueba1.service;

import com.example.Prueba1.model.Vehicle;
import com.example.Prueba1.repository.VehicleRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class VehicleService {
    private static final String[] brands = {"Toyota", "Honda", "Chevrolet", "Ford", "Nissan"};
    private static final String[] colors = {"Black", "White", "Red", "Blue", "Green"};

    private static final String[] types = {"Sedan", "Van", "SUV"};

    @Autowired
    private VehicleRepository vehicleRepository;

    /**
     * Returns all the vehicles in the database.
     *
     * @return the list of all the vehicles in the database.
     */
    public List<Vehicle> list() {
        return vehicleRepository.findAll();
    }

    /**
     * Saves a vehicle in the database.
     *
     * @param vehicle the vehicle to be saved.
     * @return the saved vehicle.
     */
    public Vehicle save(Vehicle vehicle) {
        vehicleRepository.save(vehicle);
        return vehicle;
    }

    /**
     * Retrieves a vehicle by its id.
     *
     * @param id the id of the vehicle to be retrieved.
     * @return the vehicle with the given id.
     */
    public Optional<Vehicle> getById(long id) {
        return vehicleRepository.findById(id);
    }

    /**
     * Deletes a vehicle by its id.
     *
     * @param id the id of the vehicle to be deleted.
     * @return true if the vehicle was deleted, false otherwise.
     */
    public boolean delete(long id) {
        vehicleRepository.deleteById(id);
        return true;
    }


    /**
     * Generates a random string from the given array.
     *
     * @param array the array to be used to generate a random string.
     * @return a random string from the given array.
     */
    private static String generateRandomString(String[] array) {
        return array[(int) (Math.random() * array.length)];
    }

    /**
     * Generates a random number between min and max.
     *
     * @param min minimum value
     * @param max maximum value
     * @return a random number between min and max.
     */
    private static int generateRandomNumber(int min, int max) {
        return (int) (Math.random() * (max - min)) + min;
    }


    /**
     * Generates a random boolean value.
     *
     * @return true or false randomly.
     */
    private static boolean generateYesOrNot() {
        return Math.random() < 0.5;
    }


    /**
     * Generates a random engine size for the given vehicle type.
     *
     * @param type the type of the vehicle.
     * @return a random engine size for the given vehicle type.
     */
    private static double generateEngine(String type) {
        double[] sedanEngines = {1.4, 1.6, 2.0};
        double[] vanEngines = {2.4, 3.0, 4.0};
        double[] suvEngines = {1.8, 2.2, 2.8};

        if (type.equals(types[0])) {
            return sedanEngines[(int) (Math.random() * sedanEngines.length)];
        } else if (type.equals(types[1])) {
            return vanEngines[(int) (Math.random() * vanEngines.length)];
        } else {
            return suvEngines[(int) (Math.random() * suvEngines.length)];
        }
    }

    /**
     * Generates N random vehicles and saves them in the database.
     *
     * @param N number of vehicles to be generated.
     * @return true if the vehicles were generated, false otherwise.
     */
    public boolean generateVehicles(int N) {
        for (int i = 0; i < N; i++) {
            Vehicle vehicle = new Vehicle();
            vehicle.setBrand(generateRandomString(brands));
            vehicle.setYear(generateRandomNumber(2015, 2023));
            vehicle.setColor(generateRandomString(colors));
            vehicle.setPrice(generateRandomNumber(8000000, 30000000));
            vehicle.setTurbo(generateYesOrNot());
            vehicle.setType(generateRandomString(types));
            vehicle.setEngine(generateEngine(vehicle.getType()));
            if (vehicle.getType().equals("Van")) {
                vehicle.setCabins(generateRandomNumber(1, 3));
            } else {
                vehicle.setCabins(0);
            }

            if (vehicle.getType().equals("suv")) {
                vehicle.setSunroof(generateYesOrNot());
            } else {
                vehicle.setSunroof(false);
            }
            vehicleRepository.save(vehicle);
        }
        return true;
    }

    /**
     * Filters the vehicles by the given price.
     *
     * @param price the maximum price of the vehicles to be retrieved.
     * @return the list of vehicles with a price less than or equal to the given price.
     */
    public Collection<Vehicle> filterByPrice(int price) {
        return vehicleRepository.findAll().stream()
                .filter(vehicle -> vehicle.getPrice() <= price)
                .toList();
    }

    /**
     * Filters the vehicles by the given type.
     *
     * @param type the type of the vehicles to be retrieved.
     * @return the list of vehicles with the given type.
     */
    public Collection<Vehicle> filterByType(String type) {
        return vehicleRepository.findAll().stream()
                .filter(vehicle -> vehicle.getType().equals(type))
                .toList();
    }

    /**
     * Filters the vehicles by the given brand.
     *
     * @param color the color of the vehicles to be retrieved.
     * @return the list of vehicles with the given color.
     */
    public Collection<Vehicle> filterByColor(String color) {
        return vehicleRepository.findAll().stream()
                .filter(vehicle -> vehicle.getBrand().equals(color))
                .toList();
    }

}

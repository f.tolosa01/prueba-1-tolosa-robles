package com.example.Prueba1.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "vehicles")
public class Vehicle {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "brand")
    private String brand;

    @Column(name = "year")
    private int year;

    @Column(name = "color")
    private String color;

    @Column(name = "price")
    private double price;

    @Column(name = "turbo")
    private boolean turbo;

    // the 3 type are : Sedan, Van, SUV
    @Column(name = "type")
    private String type;

    // for Sedan: 1.4cc, 1.6cc, 2.0cc
    // for Van: 2.4cc, 3.0cc, 4.0cc
    // for SUV: 1.8cc, 2.2cc, 2.8cc
    @Column(name = "engine")
    private double engine;

    // only for SUV
    // for everyone else, this value is false always
    @Column(name = "sunroof")
    private boolean sunroof;

    //only for Van
    // for everyone else, this value is 0 always
    @Column(name = "cabins")
    private int cabins;

    @Column(name = "popularity")
    private int popularity;

}

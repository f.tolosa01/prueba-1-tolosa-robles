# Pruebas de Software: Evaluación 1
Integrantes:
+ Sebastián Robles
+ Fernando Tolosa

## Descripción
### Desarrollar junto a un compañero un módulo para una automotora en alguno de los siguientes lenguajes: JS, TS, JAVA, PYTHON.  Si lo necesita, puede utilizar cualquier framework.
Este modulo debe realizar las siguientes funciones:
#### Generar N Cantidad de automóviles con estas características aleatoriamente:
+ ID: ID único al generar
+ Marca: Al menos 5
+ Año: desde 2015 hasta 2023
+ Color: al menos 5
+ Precio: desde $8.000.000 hasta $30.000.000
+ Turbo: Si o No
+ Tipo: Sedan, camioneta, SUV
+ Motor:
+ Para Sedan: 1.4cc, 1.6cc, 2.0cc
+ Para camioneta: 2.4cc, 3.0cc, 4.0cc
+ Para SUV: 1.8cc, 2.2cc, 2.8cc
+ Cabinas: 1 o 2 (SOLO CAMIONETAS)
+ Sunroof: Si o No (SOLO SUV)

#### A partir de los datos generados, se debe disponibilidad un servicio que filtre los automóviles según los siguientes filtros (combinados, o sea que si se ingresa precio y tipo debe filtrar por ambos a la misma vez):
+ Precio: Filtrará automóviles igual o menores al precio ingresado
+ Tipo: Filtrará automóviles del tipo ingresado
+ Color: Filtrará automóviles del color ingresado

#### A partir de los datos generados, se debe entregar la posibilidad de contactar a la agencia por un vehículo en particular. Al contactar, el automóvil debe subir en 1 su popularidad (aumentando en 1 un contador del mismo)

#### Se debe agregar una funcionalidad que funcione igual que el filtro del punto 2, pero si es un agente el que está consultando se le agregue la popularidad a cada automóvil. Caso contrario no se debiese mostrar.